public interface ItemList{
    public void selectItem(int index);
    public void unselectItem();
    public void add(String item);
}
