import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;

public class ItemListImpl implements ItemList{
    private List<String> list = new LinkedList<String>();
    private Window window;

    public ItemListImpl(Window window){
        this.window = window;
    }

    @Override
    public void add(String item){
        this.list.add(item);
        System.out.println(String.format("%s item is added", item));
    }

    @Override
    public void selectItem(int index){
        String item = list.get(index);
        this.window.onSelectItem(item);
    }

    @Override
    public void unselectItem(){
        this.window.onUnselectItem();
    }
}
