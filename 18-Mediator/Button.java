public interface Button{
    public void disable();
    public void enable();
}
