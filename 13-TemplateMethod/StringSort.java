public class StringSort extends SortAlgorithm{
    @Override
    public boolean lessThan(Object a, Object b){
        String s1 = (String)a;
        String s2 = (String)b;
        return s1.compareTo(s2) < 0;
    }
}
