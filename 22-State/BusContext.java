public class BusContext{
    public enum Season{
        SPRING, SUMMER, AUTUMN, WINTER
    }

    private Bus bus = new SpringBus();

    public void setSeason(Season season){
        switch(season){
        case SPRING:
            bus = new SpringBus();
            break;
        case SUMMER:
            bus = new SummerBus();
            break;
        case AUTUMN:
            bus = new AutumnBus();
            break;
        case WINTER:
            bus = new WinterBus();
            break;
        }
    }

    public void showPrice(){
        System.out.println("The price now is " + bus.getPrice());
    }
}
