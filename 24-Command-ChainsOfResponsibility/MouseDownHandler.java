public class MouseDownHandler implements Handler{
    private Handler next;

    public MouseDownHandler(Handler next){
        this.next = next;
    }

    @Override
    public void handle(Event event){
        String type = event.getType();
        if(type.equals("mouse-down")){
            System.out.println("handle mouse down event");
        } else {
            if(this.next!=null){
                this.next.handle(event);
            }
        }
    }
}
