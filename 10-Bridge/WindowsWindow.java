public class WindowsWindow implements Window{
    @Override
    public void addButton(String message){
        System.out.println("Windows window add button: "+message);
    }

    @Override
    public void addInput(String message){
        System.out.println("Windows window add input: "+message);
    }

    @Override
    public void show(){
        System.out.println("Windows window show");
    }
}

