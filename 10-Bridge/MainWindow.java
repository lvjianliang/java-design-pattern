public class MainWindow{
    private Window window;

    public MainWindow(Window window){
        this.window = window;
    }

    public void create(){
        this.window.addButton("Exit button");
        this.window.addButton("Calc button");
        this.window.addInput("Number input");
        this.window.show();
    }
}
