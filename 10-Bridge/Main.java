public class Main{
    public static void main(String[] argv){
        //创建不同平台的窗口
        Window linuxWindow = new LinuxWindow();
        Window windowsWindow = new WindowsWindow();

        //主窗口不需要关心操作系统是什么
        MainWindow mainWindow = new MainWindow(linuxWindow);
        MainWindow mainWindow2 = new MainWindow(windowsWindow);

        //不需关心操作系统
        mainWindow.create();
        mainWindow2.create();
    }
}
