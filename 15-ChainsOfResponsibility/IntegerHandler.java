public class IntegerHandler implements Handler{
    private Handler next;

    public IntegerHandler(Handler next){
        this.next = next;
    }

    @Override
    public void handle(String message){
        int value;

        //转换成数字进行处理
        try{
            value = Integer.parseInt(message);
        }catch(Exception ex){
            if(next != null){
                next.handle(message);
            }
            return;
        }

        //处理数字
        System.out.println("IntegerHandler: " + value);
    }
}
