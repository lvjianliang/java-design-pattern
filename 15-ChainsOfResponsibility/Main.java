public class Main{
    public static void main(String[] argv){
        //生成责任链
        Handler handler = null;
        handler = new OtherHandler(handler);
        handler = new FloatHandler(handler);
        handler = new IntegerHandler(handler);

        //传入整数，应该由IntegerHandler进行处理
        handler.handle("123");

        //传入小数，应该由FloatHandler进行处理
        handler.handle("123.123");
        handler.handle("123456789874312");

        //传入单词，应该由OtherHandler进行出啊里
        handler.handle("Test");
    }
}
