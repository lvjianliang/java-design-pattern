public class AppleFactoryImpl implements AppleFactory {
    @Override
    public iPhone createiPhone(int iPhoneVersion){
        switch(iPhoneVersion){
            case IPHONE_4:
                return new iPhone4();
            case IPHONE_5:
                return new iPhone5();
            default:
                throw new RuntimeException("Unknown iPhone version");
        }
    }

    @Override
    public iPad createiPad(int iPadVersion){
        switch(iPadVersion){
            case IPAD_4:
                return new iPad4();
            case IPAD_AIR:
                return new iPadAir();
            default:
                throw new RuntimeException("Unknown iPad version");
        }
    }
}
