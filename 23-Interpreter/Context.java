import java.util.HashMap;
import java.util.LinkedHashMap;

public class Context {
    private HashMap<String, Double> map = new LinkedHashMap<String, Double>();

    public void assign(String var, double value){
        this.map.put(var, value);
    }

    public double getValue(String var){
        return this.map.get(var);
    }
}
