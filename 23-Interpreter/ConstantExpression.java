public class ConstantExpression implements Expression{
    private double value;

    public ConstantExpression(double value){
        this.value=  value;
    }

    @Override
    public double eval(Context context){
        return this.value;
    }
}
