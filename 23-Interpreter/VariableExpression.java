public class VariableExpression implements Expression{
    private String varname;

    public VariableExpression(String varName){
        this.varname = varName;
    }

    @Override
    public double eval(Context context){
        return context.getValue(varname);
    }
}
