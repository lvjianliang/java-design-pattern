public class SqrtExpression implements Expression{
    private Expression a;

    public SqrtExpression(Expression a){
        this.a=a;
    }

    @Override
    public double eval(Context context){
        double a=this.a.eval(context);
        return Math.sqrt(a);
    }
}
