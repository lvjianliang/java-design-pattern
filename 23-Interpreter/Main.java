public class Main{
    public static void main(String[] argv){
        //获取求根公式
        Expression exp = getRootExpression();

        //给变量赋值
        Context context = new Context();
        context.assign("a", 1);
        context.assign("b", 5);
        context.assign("c", 4);

        //利用求根公式求解方程
        double result = exp.eval(context);
        System.out.println(result);
    }

    public static Expression getRootExpression(){
        //变量
        Expression a = new VariableExpression("a");
        Expression b = new VariableExpression("b");
        Expression c = new VariableExpression("c");

        //Delta公式
        Expression delta =
            new MinusExpression(
                new MultiplyExpression(b,b),
                new MultiplyExpression(
                    new MultiplyExpression(
                        new ConstantExpression(4),
                        a
                    ),
                    c
                )
            );

        //分子
        Expression up = 
            new MinusExpression(
                new SqrtExpression(delta),
                b
            );

        //分母
        Expression down =
            new MultiplyExpression(
                new ConstantExpression(2),
                a
            );

        //求根公式
        Expression root = new DivideExpression(up,down);

        //返回求根公式
        return root;
    }
}
