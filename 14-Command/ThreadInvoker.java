import java.util.List;
import java.util.LinkedList;
import java.util.Queue;

public class ThreadInvoker extends Thread {
    private Queue<Command> commands = new LinkedList<Command>();

    public synchronized void putCommand(Command command){
        commands.add(command);
    }

    @Override
    public void run(){
        while(true){
            //延迟1秒
            try{
                Thread.sleep(1000);
            }catch(Exception ex){
                break;
            }
            
            //如果没有命令
            if(commands.isEmpty()){
                continue;
            }

            //获取等待执行的命令
            Command command = commands.poll();

            //执行命令
            command.execute();
        }
    }
}
