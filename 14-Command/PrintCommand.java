public class PrintCommand implements Command {
    private Main receiver;
    private String message;

    public PrintCommand(Main receiver, String message){
        this.receiver = receiver;
        this.message = message;
    }

    @Override
    public void execute(){
        receiver.onPrint(message);
    }
}
