public interface Signature{
    public String getSignature();
    public Signature clone();
}
