import java.security.MessageDigest;

public class Md5Signature implements Signature{
    private String md5="";

    public Md5Signature(String message, String password){
        try{
        // 生成一个签名需要很长时间
        MessageDigest md5 = MessageDigest.getInstance("MD5");
        for(int i=0;i<10000;i++){
            this.md5 = this.bytesToString(md5.digest((this.md5 + message + password).getBytes("utf8")));
        }
        }catch(Exception ex){
            throw new RuntimeException(ex);
        }
    }

    private String bytesToString(byte[] bytes){
        String result = new String();
        for(byte e:bytes){
            result += String.format("%02x",e);
        }
        return result;
    }

    private Md5Signature(String md5){
        this.md5=md5;
    }

    @Override
    public String getSignature(){
        return this.md5;
    }

    @Override
    public Md5Signature clone(){
        return new Md5Signature(this.md5);
    }
}
