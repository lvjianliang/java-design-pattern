public interface DiscountStrategy {
    public float getPrice(float price);
}
