public class Main{
    public static void main(String[] argv){
        //创建连接池
        final ConnectionPool pool = new ConnectionPool();
        
        //创建足够多的线程测试连接池
        int threadCount = 10;
        for(int i=0;i<threadCount;i++){
            new TestThread(pool,i).start();
        }
    }
}

class TestThread extends Thread{
    private ConnectionPool pool;
    private int threadNumber;

    public TestThread(ConnectionPool pool, int threadNumber){
        this.pool = pool;
        this.threadNumber = threadNumber;
    }

    @Override
    public void run(){
        int tryCount=3;
        for(int i=0;i<tryCount;i++){
            Connection connection = pool.getConnection();
            connection.sendData(String.format("Try %03d%05d", threadNumber, i));
            connection.endWork();
        }
    }
}
