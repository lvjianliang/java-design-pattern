public interface Connection{
    public boolean isIdle();
    public void sendData(String data);
    public void startWork();
    public void endWork();
}
