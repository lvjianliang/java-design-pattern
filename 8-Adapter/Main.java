public class Main{
    public static void main(String[] argv){
        // 有一台旧的电脑，没有USB接口
        OldComputer computer = new OldComputer();

        // 现在有一个USB鼠标，想连接到旧电脑上
        USBMouse mouse = new USBMouse();

        // 因为旧电脑没有USB接口，需要将USB接口转换成ps2接口
        PS2Interface ps2 = new UsbPs2Adapter(mouse);

        // 转换完毕，可以连接到久电脑上了
        computer.connectDevice(ps2);
    }
}
