import java.util.List;
import java.util.LinkedList;

public class StateManager{
    private LinkedList<DataState> states = new LinkedList<DataState>();

    public void addState(DataState state){
        this.states.add(state);
    }

    public DataState getLastState(){
        return states.peekLast();
    }

    public void removeLastState(){
        states.remove(states.size()-1);
    }
}
