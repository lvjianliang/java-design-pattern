import java.util.List;
import java.util.LinkedList;
import java.lang.RuntimeException;

public class Database {
    private Data data = new Data();
    private StateManager manager = new StateManager();

    public void beginTransaction(){
        System.out.println("Begin transaction");
        DataState state = data.backup();
        manager.addState(state);
    }

    public void endTransaction(){
        System.out.println("End transaction");
        manager.removeLastState();
    }

    public void rollback(){
        System.out.println("Rollback");
        DataState state = manager.getLastState();
        data.restore(state);
    }

    public void insert(String x){
        this.insert(x, false);
    }

    public void insert(String x, boolean error){
        if(error){
            throw new RuntimeException();
        } else {
            data.add(x);
        }
    }

    public void show(){
        for(String each : data.getList()){
            System.out.println(each);
        }
    }
}
