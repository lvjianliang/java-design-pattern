import java.util.List;
import java.util.LinkedList;

public class Data {
    private LinkedList<String> list = new LinkedList<String>();

    public void add(String x){
        list.add(x);
    }
   
    public DataState backup(){
        List<String> list2 = (List<String>)(list.clone());
        return new DataState(list2);
    }

    public void restore(DataState state){
        list = (LinkedList<String>)state.getList();
    }
    
    public List<String> getList(){
        return list;
    }
}
