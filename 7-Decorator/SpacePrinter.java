public class SpacePrinter extends PrinterDecorator{
    public SpacePrinter(Printer printer){
        super(printer);
    }

    @Override
    public void print(String message){
        //每个字符之后增加空格
        String x="";
        for(char c : message.toCharArray()){
            x += String.valueOf(c) + " ";
        }

        //返回结果
        super.print(x);
    }
}
