public class TagPrinter extends PrinterDecorator{
    public TagPrinter(Printer printer){
        super(printer);
    }

    @Override
    public void print(String message){
        String x = "<tag>" + message + "</tag>";
        super.print(x);
    }
}
