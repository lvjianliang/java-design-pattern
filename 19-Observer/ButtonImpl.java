import java.util.List;
import java.util.LinkedList;

public class ButtonImpl implements Button{
    private List<OnClickListener> listener = new LinkedList<OnClickListener>();

    public ButtonImpl(){
    }

    public void setColor(String color){
        System.out.println("Button color is set to " + color);
    }

    @Override
    public void addOnClickListener(OnClickListener x){
        listener.add(x);
    }

    public void setPosition(int x, int y){
        System.out.println(String.format("Button position is set to %d, %d", x, y));
    }

    public void click(){
        for(OnClickListener each : listener){
            each.onClick();
        }
    }
}
