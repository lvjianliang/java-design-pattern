public class Main{
    public static void main(String[] argv){
        //添加元素
        LinkedList<Integer> list = new LinkedList<Integer>();
        list.add(3);
        list.add(2);
        list.add(8);
        
        //输出所有的元素
        Iterator<Integer> iter = list.getIterator();
        while(iter.hasNext()){
            System.out.println(iter.next());
        }
    }
}
