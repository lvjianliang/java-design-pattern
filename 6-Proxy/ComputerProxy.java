public class ComputerProxy implements Computer{
    private Computer computer;

    public ComputerProxy(Computer computer){
        this.computer=computer;
    }

    @Override
    public void startup(){
        System.out.println("ComputerProxy: startup");
        this.computer.startup();
    }

    @Override
    public void shutdown(){
        System.out.println("ComputerProxy: shutdown");
        this.computer.shutdown();
    }
}
