public class ComputerImpl implements Computer {
    private CPU cpu;
    private Harddisk harddisk;

    public ComputerImpl(){
        cpu = new CPU("Core i7");
        harddisk = new Harddisk("Seagate 1T");
    }

    public void accept(ComputerVisitor visitor){
        visitor.visit(cpu, harddisk);
    }
}
