public class CPU {
    private String type;

    public CPU(String type){
        this.type=type;
    }

    public String getType(){
        return type;
    }

    public void run(){
        System.out.println(String.format("CPU %s is running", type));
    }
}
