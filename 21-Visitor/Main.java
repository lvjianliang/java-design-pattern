public class Main{
    public static void main(String[] argv){
        Computer computer = new ComputerImpl();
        
        ComputerVisitor typeVisitor = new TypeVisitor();
        ComputerVisitor runVisitor = new RunVisitor();

        computer.accept(typeVisitor);
        computer.accept(runVisitor);
    }
}

