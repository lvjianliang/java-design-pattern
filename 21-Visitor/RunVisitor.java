public class RunVisitor implements ComputerVisitor{
    @Override
    public void visit(CPU cpu, Harddisk hd){
        cpu.run();
        hd.spin();
    }
}
