public interface iPhoneBuilder{
    public void addCpu(String cpu);
    public void addRam(String ram);
    public void addScreen(String screen);
    public iPhone build();
}
