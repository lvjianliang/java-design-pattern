public class iPhoneDirector{
    public iPhone constructiPhone4(){
        iPhoneBuilder builder = new iPhoneBuilderImpl();
        builder.addCpu("A5");
        builder.addRam("2G");
        builder.addScreen("13 inch");
        return builder.build();
    }

    public iPhone constructiPhone5(){
        iPhoneBuilder builder=new iPhoneBuilderImpl();
        builder.addCpu("A6");
        builder.addRam("4G");
        builder.addScreen("13 inch retina");
        return builder.build();
    }
}
