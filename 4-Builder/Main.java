public class Main{
    public static void main(String[] argv){
        iPhoneDirector director = new iPhoneDirector();
        iPhone p4 = director.constructiPhone4();
        iPhone p5 = director.constructiPhone5();
        p4.show();
        p5.show();
    }
}
